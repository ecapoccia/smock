package uk.ecsoft.jmock;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import uk.ecsoft.base.Java8BaseListener;
import uk.ecsoft.base.Java8Parser.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MockLocatorListener extends Java8BaseListener {

  private static final String JMOCK_TYPE_NAME = "Mock";

  private final Map<ParserRuleContext, Field> mockFieldsMap = new HashMap<>();
  private final Map<ParserRuleContext, Field> mockLocalVariablesMap = new HashMap<>();

  public Map<ParserRuleContext, Field> getMockLocalVariablesMap() {
    return Collections.unmodifiableMap(mockLocalVariablesMap);
  }

  public Map<ParserRuleContext, Field> getMockFieldsMap() {
    return Collections.unmodifiableMap(mockFieldsMap);
  }

  @Override
  public void enterFieldDeclaration(final FieldDeclarationContext ctx) {
    tryPopulateMapFromMockDeclaration(new Declaration() {
      @Override
      public ParserRuleContext parserRuleContext() {
        return ctx;
      }

      @Override
      public UnannTypeContext unannType() {
        return ctx.unannType();
      }

      @Override
      public List<VariableDeclaratorContext> variableDeclarators() {
        return ctx.variableDeclaratorList().variableDeclarator();
      }

      @Override
      public List<? extends RuleContext> modifiers() {
        return ctx.fieldModifier();
      }
    }, mockFieldsMap);
  }

  @Override
  public void enterLocalVariableDeclaration(final LocalVariableDeclarationContext ctx) {
    tryPopulateMapFromMockDeclaration(new Declaration() {
      @Override
      public ParserRuleContext parserRuleContext() {
        return ctx;
      }

      @Override
      public UnannTypeContext unannType() {
        return ctx.unannType();
      }

      @Override
      public List<VariableDeclaratorContext> variableDeclarators() {
        return ctx.variableDeclaratorList().variableDeclarator();
      }

      @Override
      public List<? extends RuleContext> modifiers() {
        return ctx.variableModifier();
      }
    }, mockLocalVariablesMap);
  }

  private interface Declaration {

    ParserRuleContext parserRuleContext();

    UnannTypeContext unannType();

    List<VariableDeclaratorContext> variableDeclarators();

    List<? extends RuleContext> modifiers();
  }

  private void tryPopulateMapFromMockDeclaration(Declaration declaration, Map<ParserRuleContext, Field> target) {
    UnannReferenceTypeContext unannReferenceTypeContext = declaration.unannType().unannReferenceType();
    if (unannReferenceTypeContext != null) {
      UnannClassOrInterfaceTypeContext unannClassOrInterfaceTypeContext = unannReferenceTypeContext.unannClassOrInterfaceType();
      if (unannClassOrInterfaceTypeContext != null) {
        String detecedType = unannClassOrInterfaceTypeContext.getText();
        if (JMOCK_TYPE_NAME.equals(detecedType)) {
          for (VariableDeclaratorContext variableDeclarator : declaration.variableDeclarators()) {
            Field field = new Field(variableDeclarator.variableDeclaratorId().getText(),
                declaration.modifiers(),
                variableDeclarator.variableInitializer());
            target.put(declaration.parserRuleContext(), field);
          }
        }
      }
    }
  }

}
