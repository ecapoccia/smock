package uk.ecsoft.jmock;

import java.util.List;
import java.util.Optional;

import static uk.ecsoft.base.Java8Parser.*;
import static uk.ecsoft.jmock.ParseTreeHelper.getLastDescendant;

class MethodInvocationTransformer {

  static String transform(MethodInvocationContext ctx) {

    MethodInvocation_lfno_primaryContext primaryContext =
        ctx.primary().primaryNoNewArray_lfno_primary().methodInvocation_lfno_primary();

    String stubOrExpects = primaryContext.Identifier().getText();

    if (!"stubs".equals(stubOrExpects) && !"expects".equals(stubOrExpects)) {
      throw new IllegalStateException("Unexpected method name to transform: " + stubOrExpects);
    }

    List<PrimaryNoNewArray_lf_primaryContext> chainedMethodCalls =
        ctx.primary().primaryNoNewArray_lf_primary();

    MethodInvocation_lf_primaryContext methodToMock = chainedMethodCalls.get(0).methodInvocation_lf_primary();
    if (!"method".equals(methodToMock.Identifier().getText())) {
      throw new IllegalStateException("Unexpected method name to mock: " + methodToMock.Identifier().getText());
    }
    String methodNameToMock = methodToMock.argumentList().getText().replace("\"", "");

    String argumentsList = "";

    if (chainedMethodCalls.size() == 2) { // with + will
      MethodInvocation_lf_primaryContext withClause = chainedMethodCalls.get(1).methodInvocation_lf_primary();
      argumentsList = getWithMethodArguments(new Method() {
        @Override
        public String getName() {
          return withClause.Identifier().getText();
        }

        @Override
        public List<ExpressionContext> getArguments() {
          return withClause.argumentList().expression();
        }
      });
    }

    StringBuilder chainedInvocation = new StringBuilder();

    String mainMethodName = ctx.Identifier().getText();

    switch (mainMethodName) {
      case "will":

        Optional<MethodInvocation_lfno_primaryContext> returnClause = getLastDescendant(ctx.argumentList().getChild(0), MethodInvocation_lfno_primaryContext.class);
        returnClause.ifPresent(m -> {
          if ("returnValue".equals(m.methodName().getText())) {
            chainedInvocation
                .append(".thenReturn(")
                .append(m.argumentList().getText())
                .append(")")
            ;
          }
          // todo! throws e altri metodi possibili
        });

        break;
      case "with": // with only

        argumentsList = getWithMethodArguments(new Method() {
          @Override
          public String getName() {
            return ctx.Identifier().getText();
          }

          @Override
          public List<ExpressionContext> getArguments() {
            return ctx.argumentList().expression();
          }
        });

        break;
      default:
        throw new IllegalStateException("Unexpected method name, expected 'will' or 'with', found: " + mainMethodName);
    }

    StringBuilder transformed = new StringBuilder();

    String mockName = primaryContext.typeName().getText();

    switch (stubOrExpects) {
      case "stubs": // when(mock.method(argument))
        generateWhenStatement(transformed, mockName, methodNameToMock, argumentsList);
        break;
      case "expects": // verify(mock).method(argument);
        generateVerifyStatement(transformed, mockName, methodNameToMock, argumentsList, getInvocationCount(primaryContext.argumentList().expression(0)));
        // expects().will() in JMock translates into distinct verify() and when() statements in Mockito
        if (!chainedInvocation.toString().isEmpty()) {
          transformed.append(";").append(" ");
          generateWhenStatement(transformed, mockName, methodNameToMock, argumentsList);
        }
        break;
      default:
        throw new IllegalStateException("Can't happen");
    }

    return transformed.append(chainedInvocation).toString();
  }

  private interface Method {
    String getName();

    List<ExpressionContext> getArguments();
  }

  private static String getWithMethodArguments(Method method) {
    StringBuilder argumentsList = new StringBuilder();
    if (!"with".equals(method.getName())) {
      throw new IllegalStateException("Unexpected method name, expected 'with', found: " + method.getName());
    }
    List<ExpressionContext> arguments = method.getArguments();
    for (ExpressionContext expression : arguments) {
      if (expression != arguments.get(0)) {
        argumentsList.append(",");
      }
      getLastDescendant(expression, MethodInvocation_lfno_primaryContext.class).ifPresent(m -> {
        String eqOrIsA = m.methodName().getText();
        switch (eqOrIsA) {
          case "eq":
            argumentsList.append(m.argumentList().getText());
            break;
          case "isA":
            argumentsList.append("any(").append(m.argumentList().getText()).append(")");
            break;
          default:
            throw new IllegalStateException("Unexpected method name, expected 'eq' or 'isA', found: " + eqOrIsA);
        }
      });
    }
    return argumentsList.toString();
  }

  private static void generateWhenStatement(StringBuilder transformed, String mockName, String methodName, String argumentsList) {
    transformed
        .append("when(")
        .append(mockName)
        .append(".")
        .append(methodName)
        .append("(")
        .append(argumentsList)
        .append("))");
  }

  private static void generateVerifyStatement(StringBuilder transformed, String mockName, String methodName, String argumentsList, String invocationCount) {
    transformed
        .append("verify(")
        .append(mockName)
        .append(invocationCount == null ? "" : ", " + invocationCount)
        .append(")")
        .append(".")
        .append(methodName)
        .append("(")
        .append(argumentsList)
        .append(")");
  }

  private static String getInvocationCount(ExpressionContext invocationCount) {
    String invocationCountText = invocationCount.getText();
    if ("once()".equals(invocationCountText)) {
      return null;
    }
    if ("twice()".equals(invocationCountText)) {
      return "times(2)";
    }
    // todo parse exactly(..)
    return null;
  }


}
