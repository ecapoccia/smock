package uk.ecsoft.jmock;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import uk.ecsoft.base.Java8Parser;

import java.util.List;

import static uk.ecsoft.base.Java8Parser.*;

class Field {

  public Field(String name, List<? extends RuleContext> modifierContexts, VariableInitializerContext variableInitializer) {
    this.name = name;
    this.modifierContexts = modifierContexts;
    this.variableInitializer = variableInitializer;
  }

  static class TokenInterval {
    public TokenInterval(Token start, Token stop) {
      this.start = start;
      this.stop = stop;
    }
    public Token start;
    public Token stop;
  }

  public String name;
  public List<? extends RuleContext> modifierContexts;
  public VariableInitializerContext variableInitializer;
  public TokenInterval declarationPoint;

}
