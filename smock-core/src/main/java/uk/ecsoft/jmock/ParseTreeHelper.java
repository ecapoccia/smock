package uk.ecsoft.jmock;

import org.antlr.v4.runtime.tree.Tree;

import java.util.Optional;

class ParseTreeHelper {

  static boolean hasDescendant(final Tree ctx, Class clazz) {
    for(int i=0, count=ctx.getChildCount(); i<count; i++) {
      if(ctx.getChild(i).getClass() == clazz || hasDescendant(ctx.getChild(i), clazz)) {
        return true;
      }
    }
    return false;
  }

  static <T extends Tree, Tr> Optional<Tr> getLastDescendant(final T ctx, Class<Tr> clazz) {
    Optional<Tr> descendant;
    for(int i=0, count=ctx.getChildCount(); i<count; i++) {
      if( ctx.getChild(i).getClass() == clazz && !hasDescendant(ctx.getChild(i), clazz) ) {
        return Optional.of((Tr)ctx.getChild(i));
      } else {
        if( (descendant = getLastDescendant(ctx.getChild(i), clazz)).isPresent()) {
          return descendant;
        }
      }
    }
    return Optional.empty();
  }

}
