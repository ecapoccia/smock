package uk.ecsoft.jmock;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ecsoft.base.Java8BaseListener;
import uk.ecsoft.base.Java8Parser;
import uk.ecsoft.base.Java8Parser.*;

import java.util.List;
import java.util.Map;

import static uk.ecsoft.jmock.MethodInvocationTransformer.transform;

import static uk.ecsoft.jmock.Field.TokenInterval;
import static uk.ecsoft.jmock.ParseTreeHelper.getLastDescendant;
import static uk.ecsoft.jmock.ParseTreeHelper.hasDescendant;

public class JmockToMockitoParseListener extends Java8BaseListener {

  private final Logger logger = LoggerFactory.getLogger(JmockToMockitoParseListener.class);

  private final TokenStreamRewriter rewriter;

  private static final String JUNIT3_TESTMETHOD_PREFIX = "test";
  private static final String JMOCK_MOCK_OBJECT_TEST_CASE = "org.jmock.MockObjectTestCase";
  private static final String JMOCK_MOCK = "org.jmock.Mock";
  private static final String JMOCK_MOCK_STATIC_FACTORY_METHOD = "mock";
  private static final String IMPORT_STATIC_ORG_MOCKITO_MOCKITO = "import static org.mockito.Mockito.*";
  private static final String RUN_WITH_MOCKITO_JUNIT_RUNNER_CLASS = "@RunWith(MockitoJUnitRunner.class)";
  private static final String MOCK_OBJECT_TEST_CASE = "MockObjectTestCase";
  private static final String JUNIT4_TEST_ANNOTATION = "@Test";

  private final TokenStream tokens;

  private final Map<ParserRuleContext, Field> mockFieldsMap;
  private final Map<ParserRuleContext, Field> mockLocalVariablesMap;

  public JmockToMockitoParseListener(Java8Parser parser, Map<ParserRuleContext, Field> mockFieldsMap, Map<ParserRuleContext, Field> mockLocalVariablesMap) {
    this.mockFieldsMap = mockFieldsMap;
    this.mockLocalVariablesMap = mockLocalVariablesMap;
    this.tokens = parser.getInputStream();
    this.rewriter = new TokenStreamRewriter(this.tokens);
  }

  public String getResult() {
    return rewriter.getText();
  }

  @Override
  public void enterNormalClassDeclaration(NormalClassDeclarationContext ctx) {
    rewriter.insertBefore(ctx.start, RUN_WITH_MOCKITO_JUNIT_RUNNER_CLASS + " ");
    if (ctx.superclass() != null) {
      if (MOCK_OBJECT_TEST_CASE.equals(ctx.superclass().classType().Identifier().toString())) {
        rewriter.delete(ctx.superclass().start, ctx.superclass().stop);
        logger.debug("Discarded {} as base class", MOCK_OBJECT_TEST_CASE);
      }
    }
  }

  @Override
  public void enterFieldDeclaration(final FieldDeclarationContext ctx) {
    if (mockFieldsMap.containsKey(ctx)) {
      // mark insertion point for field
      mockFieldsMap.get(ctx).declarationPoint = new TokenInterval(ctx.start, ctx.stop);
    }
  }

  @Override
  public void enterLocalVariableDeclaration(final LocalVariableDeclarationContext ctx) {
    if (mockLocalVariablesMap.containsKey(ctx)) {
      mockLocalVariablesMap.get(ctx).declarationPoint = new TokenInterval(ctx.start, ctx.stop);
    }
  }

  @Override
  public void enterMethodDeclaration(MethodDeclarationContext ctx) {
    if (ctx.methodHeader().methodDeclarator().Identifier().toString().startsWith(JUNIT3_TESTMETHOD_PREFIX)) {
      rewriter.insertBefore(ctx.start, JUNIT4_TEST_ANNOTATION + " ");
    }
  }

  @Override
  public void enterStatementWithoutTrailingSubstatement(StatementWithoutTrailingSubstatementContext ctx) {
    if (hasDescendant(ctx, StatementWithoutTrailingSubstatementContext.class)) {
      return;
    }
    getLastDescendant(ctx, MethodInvocationContext.class).ifPresent(m -> {
      try {
        String transformedMethod = transform(m);
        rewriter.replace(m.start, m.stop, transformedMethod);
        logger.debug("Transforming [{}] into [{}]", m.getText(), transformedMethod);
      } catch (NullPointerException | IllegalStateException e) {
        // swallow and continue, as the mock might have been declared elsewhere;
        // if the method is transformable, it is assumed to be a jmock mock
      }
    });
  }

  @Override
  public void enterImportDeclaration(ImportDeclarationContext ctx) {
    SingleTypeImportDeclarationContext singleTypeImportDeclaration = ctx.singleTypeImportDeclaration();
    if (singleTypeImportDeclaration != null) {
      if (JMOCK_MOCK.equals(tokens.getText(singleTypeImportDeclaration.typeName().getPayload()))) {
        rewriter.replace(ctx.start, ctx.stop, IMPORT_STATIC_ORG_MOCKITO_MOCKITO);
        return;
      }
      if (JMOCK_MOCK_OBJECT_TEST_CASE.equals(tokens.getText(singleTypeImportDeclaration.typeName().getPayload()))) {
        rewriter.delete(ctx.start, ctx.stop);
      }
    }
  }

  @Override
  public void enterArgumentList(ArgumentListContext ctx) {
    List<ExpressionContext> arguments = ctx.expression();
    for (ExpressionContext argument : arguments) {
      getLastDescendant(argument, CastExpressionContext.class)
          .flatMap(castExpression -> getLastDescendant(castExpression, MethodInvocation_lfno_primaryContext.class))
          .ifPresent(method -> {
            String fieldName = method.typeName().getText();
            String methodName = method.Identifier().getText();
            if ("proxy".equals(methodName)) {
              rewriter.replace(argument.start, argument.stop, fieldName);
            }
          });
    }
  }

  @Override
  public void exitCompilationUnit(CompilationUnitContext ctx) {

    // emit fields
    for (Map.Entry<ParserRuleContext, Field> field : mockFieldsMap.entrySet()) {
      StringBuilder replacement = new StringBuilder("@Mock ");
      field.getValue().modifierContexts.stream().forEach(m -> replacement.append(m.getText()).append(" "));
      VariableInitializerContext variableInitializer = field.getValue().variableInitializer;
      MethodInvocation_lfno_primaryContext primaryInvocation =
          getLastDescendant(variableInitializer.expression(), MethodInvocation_lfno_primaryContext.class).get();
      if (!JMOCK_MOCK_STATIC_FACTORY_METHOD.equals(tokens.getText(primaryInvocation.methodName()))) {
        throw new IllegalStateException("Must init mock in static mock(class) method!");
      }
      //get Type from parameter list
      String typeName =
          getLastDescendant(primaryInvocation.argumentList().expression().get(0), TypeNameContext.class).get().Identifier().getText();
      replacement
          .append(typeName)
          .append(" ")
          .append(field.getValue().name)
          .append(";");
      // rewrite declaration
      rewriter.replace(field.getValue().declarationPoint.start, field.getValue().declarationPoint.stop, replacement.toString());
    }

    // emit local variables
    for (Map.Entry<ParserRuleContext, Field> field : mockLocalVariablesMap.entrySet()) {
      StringBuilder replacement = new StringBuilder();
      field.getValue().modifierContexts.stream().forEach(m -> replacement.append(m.getText()).append(" "));
      VariableInitializerContext variableInitializer = field.getValue().variableInitializer;
      MethodInvocation_lfno_primaryContext primaryInvocation =
          getLastDescendant(variableInitializer.expression(), MethodInvocation_lfno_primaryContext.class).get();
      if (!JMOCK_MOCK_STATIC_FACTORY_METHOD.equals(tokens.getText(primaryInvocation.methodName()))) {
        throw new IllegalStateException("Must init mock in static mock(class) method!");
      }
      //get Type from parameter list
      String typeName =
          getLastDescendant(primaryInvocation.argumentList().expression().get(0), TypeNameContext.class).get().Identifier().getText();
      replacement
          .append(typeName)
          .append(" ")
          .append(field.getValue().name)
          .append(" = mock(")
          .append(typeName)
          .append(".class)");
      // rewrite declaration
      rewriter.replace(field.getValue().declarationPoint.start, field.getValue().declarationPoint.stop, replacement.toString());
    }
  }

}
