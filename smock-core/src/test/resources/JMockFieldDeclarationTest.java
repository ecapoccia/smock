package my.pack;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.util.Collections;

public class JMockFieldDeclarationTest extends MockObjectTestCase {

  public void testMethod1() {
    mockedInterface.expects(once()).method("compareTo").with(isA(Integer.class)).will(returnValue(0));
  }

  public void testMethod2() {
    int a = 0;
    Mock mockedLocalInterface = mock(Comparator.class);
    a++;
    mockedLocalInterface.expects(once()).method("compareTo").with(isA(Integer.class));
    callSomethingWith((Comparator)mockedLocalInterface.proxy());
  }

  public JMockFieldDeclarationTest() {
    System.out.println("No-Args constructor");
  }

  private int integerField;

  private Mock mockedInterface = mock(Comparator.class);

  @Override
  protected void setUp() throws Exception {
    /* block comment */
    super.setUp();
    mockedInterface.stubs().method("compareTo").with(eq(2)).will(returnValue(1));    // line comment
    anotherMockedInterfaceNotDeclaredHere.stubs().method("compareTo").with(eq(2)).will(returnValue(1));
    int localVar = 3;
    for (int i = 0; i < localVar; i++) {
      List<Integer> listToSort = new List<>();
      Collections.sort(listToSort, (Comparator)mockedInterface.proxy());
      Collections.sort(listToSort, (Comparator) anotherMockedInterfaceNotDeclaredHere.proxy());
    }
  }
}