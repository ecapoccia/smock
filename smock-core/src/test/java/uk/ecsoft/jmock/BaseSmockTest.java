package uk.ecsoft.jmock;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import uk.ecsoft.base.Java8BaseListener;
import uk.ecsoft.base.Java8Lexer;
import uk.ecsoft.base.Java8Listener;
import uk.ecsoft.base.Java8Parser;

/**
 * Created by emiliano on 8/19/15.
 */
public class BaseSmockTest<T> {

  private final String prefix;
  private final String postfix;

  public BaseSmockTest(String prefix, String postfix) {
    this.prefix = prefix;
    this.postfix = postfix;
  }

  protected void visit(String testText, Java8Listener listener) {
    Java8Lexer lexer = new Java8Lexer(new ANTLRInputStream(prefix + testText + postfix));
    CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
    Java8Parser parser = new Java8Parser(commonTokenStream);
    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();

    ParseTreeWalker walker = new ParseTreeWalker();
    walker.walk(listener, compilationUnitContext);
  }

  protected abstract static class AbstractTestListener<T> extends Java8BaseListener {
    protected T result;
    public T getResult() {
      return result;
    }
  }

}
