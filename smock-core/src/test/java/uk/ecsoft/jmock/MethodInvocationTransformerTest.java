package uk.ecsoft.jmock;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static uk.ecsoft.base.Java8Parser.MethodInvocationContext;

/**
 * Created by emiliano on 8/18/15.
 */
public class MethodInvocationTransformerTest extends BaseSmockTest {

  private AbstractTestListener<String> listener;

  @Before
  public void setup() {
    listener = new AbstractTestListener<String>() {
      @Override
      public void enterMethodInvocation(MethodInvocationContext ctx) {
        result = MethodInvocationTransformer.transform(ctx);
      }
    };
  }

  public MethodInvocationTransformerTest() {
    super("class Test { void test() { ", " } }");
  }

  @Test
  public void shouldGenerateVerifyStatementOnExpects() {
    visit("mockedInterface.expects(once()).method(\"compareTo\").with(eq(0));", listener);
    assertEquals("verify(mockedInterface).compareTo(0)", listener.getResult());
  }

  @Test
  public void shouldGenerateWhenStatementOnStubs() {
    visit("mockedInterface.stubs().method(\"compareTo\").with(eq(0)).will(returnValue(1));", listener);
    assertEquals("when(mockedInterface.compareTo(0)).thenReturn(1)", listener.getResult());
  }

  @Test
  public void shouldGenerateBothWhenAndModifyStatementsOnExpectsFollowedByWill() {
    visit("mockedInterface.expects(once()).method(\"compareTo\").with(eq(0)).will(returnValue(1));", listener);
    assertEquals("verify(mockedInterface).compareTo(0); when(mockedInterface.compareTo(0)).thenReturn(1)", listener.getResult());

  }

  @Test
  public void shouldRespectTwiceInvocationCountClauseOnExpects() {
    visit("mockedInterface.expects(twice()).method(\"compareTo\").with(eq(0));", listener);
    assertEquals("verify(mockedInterface, times(2)).compareTo(0)", listener.getResult());
  }

  @Test
  public void shouldHandleMultipleArguments() {
    visit("mockedInterface.expects(twice()).method(\"compareTo\").with(eq(0), eq(1), isA(Integer.class));", listener);
    assertEquals("verify(mockedInterface, times(2)).compareTo(0,1,any(Integer.class))", listener.getResult());
  }

}