package uk.ecsoft.jmock;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;
import uk.ecsoft.base.Java8Lexer;
import uk.ecsoft.base.Java8Parser;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by emiliano on 8/11/15.
 */
public class BasicTest {


  @Test
  public void shouldGeneratePlainClass() throws IOException {
    InputStream testClass = getClass().getClassLoader().getResourceAsStream("JMockFieldDeclarationTest.java");
    Java8Lexer lexer = new Java8Lexer(new ANTLRInputStream(testClass));
    CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
    Java8Parser parser = new Java8Parser(commonTokenStream);
    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();

    ParseTreeWalker walker = new ParseTreeWalker();
    MockLocatorListener listener = new MockLocatorListener();
    walker.walk(listener, compilationUnitContext);

    walker = new ParseTreeWalker();
    JmockToMockitoParseListener mainListener = new JmockToMockitoParseListener(parser, listener.getMockFieldsMap(), listener.getMockLocalVariablesMap());
    walker.walk(mainListener, compilationUnitContext);

    System.out.println(mainListener.getResult());

  }

}
