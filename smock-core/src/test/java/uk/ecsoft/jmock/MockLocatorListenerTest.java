package uk.ecsoft.jmock;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MockLocatorListenerTest extends BaseSmockTest {


  public MockLocatorListenerTest() {
    super("class Test { ", " }");
  }

  @Test
  public void shouldPopulateFieldsMapWithMockFieldDeclaration() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("Mock mockedLocalInterface = mock(Comparator.class);", testListener);
    assertEquals(1, testListener.getMockFieldsMap().size());
    assertEquals(0, testListener.getMockLocalVariablesMap().size());
    assertEquals("mockedLocalInterface", testListener.getMockFieldsMap().values().iterator().next().name);
  }

  @Test
  public void shouldPopulateFieldsMapWithMultipleMockFieldDeclarations() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("Mock mock1 = mock(Comparator.class);Mock mock2 = mock(Comparator.class);", testListener);
    assertEquals(2, testListener.getMockFieldsMap().size());
  }

  @Test
  public void shouldPopulateFieldsMapWithMockFieldDeclarationOnly() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("int another;Mock mock1 = mock(Comparator.class);String yetAnother=\"another string\";", testListener);
    assertEquals(1, testListener.getMockFieldsMap().size());
  }

  @Test
  public void shouldPopulateLocalVariablesMapWithMockFieldDeclaration() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("void medhod() { Mock mockedLocalInterface = mock(Comparator.class); }", testListener);
    assertEquals(0, testListener.getMockFieldsMap().size());
    assertEquals(1, testListener.getMockLocalVariablesMap().size());
    assertEquals("mockedLocalInterface", testListener.getMockLocalVariablesMap().values().iterator().next().name);
  }

  @Test
  public void shouldPopulateLocalVariablesMapWithMultipleMockFieldDeclarations() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("void medhod() { Mock mock1 = mock(Comparator.class);Mock mock2 = mock(Comparator.class); }", testListener);
    assertEquals(2, testListener.getMockLocalVariablesMap().size());
  }

  @Test
  public void shouldPopulateLocalVariablesMapWithMockFieldDeclarationOnly() throws Exception {
    MockLocatorListener testListener = new MockLocatorListener();
    visit("void medhod() { int another;Mock mock1 = mock(Comparator.class);String yetAnother=\"another string\"; }", testListener);
    assertEquals(1, testListener.getMockLocalVariablesMap().size());
  }

}